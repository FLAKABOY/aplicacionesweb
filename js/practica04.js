function hacerPeticion(){
    //Obtener el valor del id a buscar
    let id = document.getElementById("id").value;

    //Declarar el objeto para hacer la peticion
    const http = new XMLHttpRequest;

    //Declarar el url dob=nde se obtendra el json
    const url = "https://jsonplaceholder.typicode.com/users/" + id;

    //Variable para rastrear si la alerta ya se mostro en la validacion
    let alertaMostrada = false;
    console.log(url);

    // Validar la respuesta
    http.onreadystatechange = function () {
        console.log("readyState:" + this.readyState + " status:" + this.status);

        // Mostrar la alerta solo si la solicitud ha finalizado y el estado no es 200
        if (this.readyState == 4 && this.status !== 200) {
            if (!alertaMostrada) {
                alert("EL ID NO EXISTE O NO SE ESTABLECIÓ CONEXIÓN CON EL SERVIDOR");
                alertaMostrada = true;
            }
        }

        // Si la solicitud se ha completado con éxito, manejar la respuesta
        if(this.status == 200 && this.readyState == 4){
            //obtener el objeto para dibujar el json
            let res = document.getElementById("lista");
            //Convertir el texto plano a un objeto json
            const json = JSON.parse(this.responseText);
            //Mostrar la info del json en el html
            res.innerHTML += '<tr> <td class = "columna1">' + json.id + '</td>'
            + '<td class="columna2">' + json.name + '</td>'
            + '<td class="columna3">' + json.username + '</td>'
            + '<td class="columna4">' + json.email + '</td>'
            //Mostrar atributo que es un objeto
            + '<td class="columna5">' + json.address.street + ", " + json.address.suite +  " " + json.address.city +  " " + json.address.zipcode
                +  " lat:" + json.address.geo.lat +  " long:" + json.address.geo.lng +'</td>'
            + '<td class="columna6">' + json.phone + '</td>'
            + '<td class="columna7">' + json.website + '</td>'
            + '<td class="columna8">' + json.company.name + " " + json.company.catchPhrase + " " + json.company.bs + '</td>'
            + ' </tr>';
            res.innerHTML += "</tbody>"
        }
    };
    http.open('GET',url,true);
    http.send();
}

//Codificar el boton
document.getElementById("btnCargar").addEventListener("click",function () {
    //Limpiar la tabla para otra peticion
    let res = document.getElementById("lista");
    res.innerHTML = "";
    //Llamar a la funcion para mostrar la busqueda
    hacerPeticion();
});