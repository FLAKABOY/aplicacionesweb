//Practica 02 uso de objeto fetch
//Manejando promesas
//Manejando await

llamadoFetch = () => {
    const url = "https://jsonplaceholder.typicode.com/todos";
    fetch(url)
        .then(respuesta => respuesta.json())
        .then(data => mostrarTodos(data))
        .catch((reject) => {
            console.log("Algo ha salido mal" + reject);
        });
}

//Usando await
const llamadoAwait = async () => {
    try {
        const url = "https://jsonplaceholder.typicode.com/todos";
        const respuesta = await fetch(url);
        const data = await respuesta.json();
        mostrarTodos(data);
    }
    catch (error) {
        console.log('Ha habido un error' + error);
    }
}

const mostrarTodos = (data) => {
    console.log(data);

    const res = document.getElementById('respuesta');
    res.innerHTML = "";

    for (let item of data) {
        //La notación ${item.userId} se utiliza para evitar el uso de "+" al concatenar el texto
        res.innerHTML += `<div>
                            <span>User ID:</span> ${item.userId}, 
                            <span>ID:</span> ${item.id}, 
                            <span>Title:</span> ${item.title},
                            <span>Completed:</span> ${item.completed} 
                        </div>`;
    }
}

//Codificar el boton
//Cargar por promesa
document.getElementById('btnCargarP').addEventListener('click', function () {
    llamadoFetch();
});

//Cargar por await
document.getElementById('btnCargarA').addEventListener('click', function () {
    llamadoAwait();
});

//Limpiar campos
document.getElementById('btnLimpiar').addEventListener('click', function () {
    const res = document.getElementById('respuesta');
    res.innerHTML = "";

});