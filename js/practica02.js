var idB = document.getElementById("id");
function hacerPeticion() {
    var id = idB.value;
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/albums/" + id;

    // Variable para rastrear si la alerta ya se mostró
    let alertaMostrada = false;

    //Validar la respuesta
    http.onreadystatechange = function () {
        console.log("readyState:" + this.readyState + " status:" + this.status);

        // Mostrar la alerta solo si la solicitud ha finalizado y el estado no es 200
        if (this.readyState == 4 && this.status !== 200) {
            if (!alertaMostrada) {
                alert("EL ID NO EXISTE O NO SE ESTABLECIÓ CONEXIÓN CON EL SERVIDOR");
                alertaMostrada = true;
            }
        }
        if (this.status == 200 && this.readyState == 4) {
            //Aqui se dibuja la agina
            let res = document.getElementById("lista");
            //Obtener el json de la peticion
            const json = JSON.parse(this.responseText);
            //Mostrar la informacion del json en el html
            res.innerHTML += '<tr>' /*'<tr> <td class = "columna1">' + json.userId + '</td>'
               / + '<td class = "columna2">' + json.id + '</td>'*/
                + '<td class = "columna3">' + json.title + '</td> </tr>';
            res.innerHTML += "</tbody>";
        }
    };
    http.open('GET', url, true);
    http.send();
}

//Codificacion de los botones
document.getElementById("btnCargar").addEventListener("click", function () {
    let res = document.getElementById("lista");
    res.innerHTML = "";
    hacerPeticion();
});

